<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt"%>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>GSON Repo Commits</title>
    </head>
    <body>
        <table>
            <tr>
                <th>commit hash</th>
                <th>comments</th>
                <th>author</th>
                <th>date</th>
            </tr>
            <c:forEach items="${gitcommits}" var="list">
                <tr>                    
                    <c:set var = "sha" value = "${list.sha}" />
                    <c:set var = "len" value = "${fn:length(sha)}" />
                    <c:set var = "lastChar" value = "${fn:substring(sha,len-1,len)}" />
                    <c:if test = "${lastChar >= 'a' && lastChar <= 'f' }">
                        <c:choose>
                            <c:when test="${lastChar == 'a'}">
                                <c:set var="lastChar" value="10" />
                            </c:when>
                            <c:when test="${lastChar == 'b'}">
                                <c:set var="lastChar" value="11" />
                            </c:when>
                            <c:when test="${lastChar == 'c'}">
                                <c:set var="lastChar" value="12" />
                            </c:when>
                            <c:when test="${lastChar == 'd'}">
                                <c:set var="lastChar" value="13" />
                            </c:when>
                            <c:when test="${lastChar == 'e'}">
                                <c:set var="lastChar" value="14" />
                            </c:when>
                            <c:when test="${lastChar == 'f'}">
                                <c:set var="lastChar" value="15" />
                            </c:when>
                            <c:otherwise>
                                <c:set var="lastChar" value="${lastChar}" /> 
                            </c:otherwise>
                        </c:choose>                       
                    </c:if>                  
                    <fmt:parseNumber var = "shanum" type = "number" value = "${lastChar}" />
                    <c:choose>
                        <c:when test="${shanum % 2 == 0}">
                            <c:set var="color" value="#800000" />
                        </c:when>
                        <c:otherwise>
                            <c:set var="color" value="#FFDF00" />
                        </c:otherwise>
                    </c:choose>
                    <td bgcolor="<c:out value="${color}"/>">${list.sha}</td>
                    <td bgcolor="<c:out value="${color}"/>">${list.comments}</td>
                    <td bgcolor="<c:out value="${color}"/>">${list.author}</td>
                    <td bgcolor="<c:out value="${color}"/>">${list.date}</td>                  
                </tr>
            </c:forEach> 
        </table>   
    </body>
</html>