/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.Objects;

public class Commit {

    private String sha;
    private String author;
    private String comments;
    private String date;  //YYYY-MM-DDTHH:MM:SSZ returned

    public Commit(String sha, String author, String comments, String date) {
        this.sha = sha;
        this.author = author;
        this.comments = comments;
        this.date = date;
    }

    public String getSha() {
        return sha;
    }

    public String getAuthor() {
        return author;
    }

    public String getComments() {
        return comments;
    }

    public String getDate() {
        return date;
    }

    public void setSha(String sha) {
        this.sha = sha;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.sha);
        hash = 79 * hash + Objects.hashCode(this.author);
        hash = 79 * hash + Objects.hashCode(this.comments);
        hash = 79 * hash + Objects.hashCode(this.date);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Commit other = (Commit) obj;
        if (!Objects.equals(this.sha, other.sha)) {
            return false;
        }
        if (!Objects.equals(this.author, other.author)) {
            return false;
        }
        if (!Objects.equals(this.comments, other.comments)) {
            return false;
        }
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Commit{" + "sha=" + sha + ", author=" + author + ", comments=" + comments + ", date=" + date + '}';
    }
}
