package controller;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import service.CommitsService;

public class CommitsController extends SimpleFormController {

    private CommitsService commitsService;

    public void setCommitsService(CommitsService commitsService) {
        this.commitsService = commitsService;
    }

    public CommitsController() {
        setCommandClass(Login.class);
        setCommandName("name");
        setSuccessView("commitsView");
        setFormView("loginView");
    }

    @Override
    protected ModelAndView onSubmit(Object command) throws Exception {        
        ModelAndView mv = new ModelAndView(getSuccessView());
        mv.addObject("gitcommits", commitsService.retrieveCommits());
        return mv;
    }
}
