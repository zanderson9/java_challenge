package service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;
import com.jcabi.github.RtGithub;
import com.jcabi.github.Github;
import com.jcabi.github.Repo;
import com.jcabi.github.RepoCommits;
import com.jcabi.github.Coordinates;
import com.jcabi.github.RepoCommit;
import controller.Commit;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.JsonObject;

@Service
public class CommitsService {

    private static final List<Commit> commits = new ArrayList<Commit>();    

    public List<Commit> retrieveCommits() {
        Github github = new RtGithub("javachallenge9", "javachallenge12");
        Repo repo = github.repos().get(new Coordinates.Simple("google", "gson"));
        RepoCommits repoCommits = repo.commits();
        List<RepoCommit> listOfCommits = mockRetrieveCommits(repoCommits);
        commits.clear();
        for (RepoCommit commit : listOfCommits) {
            try {
                JsonObject jo = commit.json();
                Commit acommit = getCommitFromJSON(commit, jo);
                commits.add(acommit);
            } catch (IOException ex) {
                Logger.getLogger(CommitsService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return commits;
    }

    private Commit getCommitFromJSON(RepoCommit commit, JsonObject jo) {
        String sha = commit.sha();
        Map acommit = ((Map) jo.get("commit"));
        Map author = ((Map) acommit.get("author"));
        String authorName = author.get("name").toString();
        String date = author.get("date").toString();
        String comments = "";

        return new Commit(sha, authorName, comments, date);
    }

    private List<RepoCommit> mockRetrieveCommits(RepoCommits repoCommits) {
        List<RepoCommit> listOfCommits = new ArrayList<RepoCommit>();
        listOfCommits.clear();
        RepoCommit c = repoCommits.get("da5cae371e8b739fe63a6c6d16debf7b297dea0e");
        listOfCommits.add(c);
        c = repoCommits.get("aa1a34eb5e1f0070b2b5697434a358059a4d7267");
        listOfCommits.add(c);
        c = repoCommits.get("5bbc768fa6cd3e8a3a7bebf52b0ac1e4c6e0bd12");
        listOfCommits.add(c);
        c = repoCommits.get("27c933527572bf861b8cb45774b0ceaa48b8805c");
        listOfCommits.add(c);
        c = repoCommits.get("b046ea28eeb819ecc30c3a39cb6912dc84fae015");
        listOfCommits.add(c);
        c = repoCommits.get("3f4ac29f9112799a7374a99b18acabd0232ff075");
        listOfCommits.add(c);
        c = repoCommits.get("ca9ae4c288b5f4519f2166f42a2efac64fdd7834");
        listOfCommits.add(c);
        c = repoCommits.get("86ade21078b656506c47d51933af6df921ab5161");
        listOfCommits.add(c);
        c = repoCommits.get("92b617379191bec5bc498830c87b0c143975848c");
        listOfCommits.add(c);
        c = repoCommits.get("d8d8ccb98a80108d4f08f7f5e92a4af5f757a27e");
        listOfCommits.add(c);
        c = repoCommits.get("c1e7e2d2808b042cbe47ca31869ee6ccc62c5417");
        listOfCommits.add(c);
        c = repoCommits.get("67ff7d519826279d2e1f24ae6bf98c0a5b394f2d");
        listOfCommits.add(c);
        c = repoCommits.get("9d44cbc19a73b45971c4ecb33c8d34d673afa210");
        listOfCommits.add(c);
        c = repoCommits.get("35c0ea746874472b5a85acc5f07c859765efb6cb");
        listOfCommits.add(c);
        c = repoCommits.get("5184e717cef322c239b59ea846b89cd5ead423f2");
        listOfCommits.add(c);
        c = repoCommits.get("986d3fb2fa27a117ca0566896f99e7b5a0fd5b2a");
        listOfCommits.add(c);

        return listOfCommits;
    }
}
