/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.Test;
import com.jcabi.github.Github;
import com.jcabi.github.Coordinates;
import com.jcabi.github.Repos.RepoCreate;
import com.jcabi.github.mock.MkGithub;
import java.io.IOException;
import java.util.Date;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;

/**
 *
 * @author Zoya
 */
public class TestGitHubAPI {

    public TestGitHubAPI() {
    }

    @Test
    public void testCreatesIssueAndPostsMessage() throws IOException {
        Github github = new MkGithub();
        RepoCreate rc = new RepoCreate("testRepo", false);
        github.repos().create(rc);
        Coordinates coord = new Coordinates.Simple("jeff", "testRepo");
        MatcherAssert.assertThat(
                github.repos().get(coord)
                      .issues().get(1).comments().iterate(new Date()),
                Matchers.emptyIterable()
        );
    }
}
