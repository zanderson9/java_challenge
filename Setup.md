# Java Challenge

**GitHubGSON**, a web application to retrieve and display GitHub google/gson commits.

### Prerequisites

The following software was used to develop and build  **GitHubGSON**:

- [NetBeans IDE 8.2 with Java EE bundle for web](https://netbeans.org/downloads/)
    - GlassFish Server 4.1.1
    - Spring Web MVC 3.2.7
- [object-oriented Java adapter for GitHub API](https://github.jcabi.com/)

### Installing

For the purpose of this assignment, a straightforward way to run **GitHubGSON** is to import its web archive WAR file into NetBeans IDE and select "Run" from the IDE menu.

1.  Download and Save **GitHubGSON.war** file somewhere on disk, for example, create folder C:\code and save **GitHubGSON.war** in there.  Note, **GitHubGSON.war** is included with the Pull Request
2. Unzip **GitHubGSON.war** (any zip/unzip utility works)
3. Navigate to unzipped project folder, for example, C:\code\GitHubGSON
4. Inside the project folder, create a folder called **web**
5. Move the WEB-INF folder into that **web** folder. After the move, the project's folder structure may look like this:
  ![](images/PrjDir.jpg)
 Note, step 5 is included in order to avoid Netbeans WAR unpack error, "Web Pages folder overlaps Project folder".

6. Open NetBeans IDE

7. Select File->New Project->Java Web->Web Application with Existing Sources.  Click Next
![](images/Step1.jpg)

8. In this dialog, populate "Location" by navigating to the location of the project
sources and web files, for example,
**C:\CODE\GitHubGSON\web**. Other entries will be populated by the
Wizard based on the "Location". Click Next ![](images/Step2.jpg)

9. Select GlassFish Server. "Context Path" should be set to **/GitHubGSON**.
Click Next ![](images/Step3.jpg)

10. If Warning appears in the Next step, you can choose "Ignore" ![](images/Warning.jpg)

11. All done. **GitHubGSON** project is now imported and will be displayed
in "Projects" tab

12. While in "Projects" tab, right-click on **GitHubGSON**,
choose "Properties", and select "Run" under "Categories".
Type **/commits.htm** under "Relative URL" entry.
Click "OK" ![](images/PrjPropsRun.jpg)

13. Start GlassFish server: Select "Services" tab in IDE,
expand "Servers" entry, right-click on **GlassFish Server** and select "Start".
Observe messages in the IDE "Output" window to confirm that the server started![](images/StartServer.jpg)

14. Return to "Projects" tab, select **GitHubGSON** in Projects tree (mouse-click to highlight it), then click "Run" from the main IDE menu

15. NetBeans will build and deploy **GitHubGSON**

16. **Login** page will appear. Type any text into the textbox (or leave it blank), and click OK ![](images/LoginPage.jpg)

17. **Result** page will appear ![](images/Result.jpg)



### GitHub Third-Party API library
To access GitHub from Java code, I selected a third-party API, [jacabi-github](https://github.jcabi.com/apidocs-0.41/index.html). Note, the jar file for this API is included with the Pull Request.
Although there are several Java SDK-s that wrap and expose the GitHub API, but the [jacabi-github](https://github.jcabi.com/) offers several advantages that attracted me, as follows:
- it is truly object-oriented, so that GitHub itself and its comprising parts are viewed as an Object
- it has its built-in mock engine that allows to create reliable unit tests, which can lead to an **effective TDD** (Test-Driven-Development). I wrote a sample unit test and included it with the Pull Request, see **source/test/TestGitHubAPI.java**
- it is based on JSR-353 and is extensible
- support for Maven is provided
